package main

import (
	"os"
	"path/filepath"
	"sort"
	"strings"
	"testing"
)

const fixtureDirectory = `fixtures`

func TestConvertFixtures(t *testing.T) {
	fh, err := os.Open(fixtureDirectory)
	if err != nil {
		t.Fatal(err)
	}
	defer fh.Close()

	fixtures, err := fh.Readdirnames(-1)
	if err != nil {
		t.Fatal(err)
	}

	sort.Strings(fixtures)

	for _, fixture := range fixtures {
		if !strings.HasSuffix(fixture, `.php`) {
			continue
		}

		ret, err := ConvertFile(filepath.Join(fixtureDirectory, fixture))
		if err != nil {
			t.Errorf("In test fixture %s:\n %s", fixture, err.Error())
			continue
		}

		// Success
		t.Logf("Successful test for fixture %s:\n%s", fixture, ret)
	}

}
