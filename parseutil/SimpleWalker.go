package parseutil

import (
	"fmt"

	"github.com/z7zmey/php-parser/node"
	"github.com/z7zmey/php-parser/walker"
)

type simpleWalker struct {
	cb      func(node.Node) error
	lastErr error
}

func (mw *simpleWalker) EnterNode(w walker.Walkable) bool {
	n, ok := w.(node.Node)
	if !ok {
		mw.lastErr = fmt.Errorf("Tried to walk non-node '%t'", w)
		return false
	}

	err := mw.cb(n)
	if err != nil {
		mw.lastErr = err
		return false
	}

	return true
}

func (mw *simpleWalker) LeaveNode(w walker.Walkable)                  {}
func (mw *simpleWalker) EnterChildNode(key string, w walker.Walkable) {}
func (mw *simpleWalker) LeaveChildNode(key string, w walker.Walkable) {}
func (mw *simpleWalker) EnterChildList(key string, w walker.Walkable) {}
func (mw *simpleWalker) LeaveChildList(key string, w walker.Walkable) {}

var _ walker.Visitor = &simpleWalker{} // interface assertion

func SimpleWalk(n node.Node, cb func(node.Node) error) error {
	mw := simpleWalker{cb: cb}
	n.Walk(&mw)

	return mw.lastErr
}
