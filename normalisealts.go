package main

import (
	"github.com/z7zmey/php-parser/node"
	"github.com/z7zmey/php-parser/node/stmt"
)

func normaliseAltCb(n_ *node.Node) error {

	switch n := (*n_).(type) {
	case *stmt.AltIf:
		// Build replacement node
		ifStmt := stmt.NewIf(n.Cond, n.Stmt, n.ElseIf, n.Else)
		ifStmt.FreeFloating = n.FreeFloating
		ifStmt.Position = n.Position
		// ifStmt has no .Attributes prop

		*n_ = ifStmt

	case *stmt.AltElse:
		elseStmt := stmt.NewElse(n.Stmt)
		elseStmt.FreeFloating = n.FreeFloating
		elseStmt.Position = n.Position

		*n_ = elseStmt

	case *stmt.AltElseIf:
		elifStmt := stmt.NewElseIf(n.Cond, n.Stmt)
		elifStmt.FreeFloating = n.FreeFloating
		elifStmt.Position = n.Position

		*n_ = elifStmt

	case *stmt.AltFor:
		forStmt := stmt.NewFor(n.Init, n.Cond, n.Loop, n.Stmt)
		forStmt.FreeFloating = n.FreeFloating
		forStmt.Position = n.Position

		*n_ = forStmt

	case *stmt.AltForeach:
		feStmt := stmt.NewForeach(n.Expr, n.Key, n.Variable, n.Stmt)
		feStmt.FreeFloating = n.FreeFloating
		feStmt.Position = n.Position

		*n_ = feStmt

	case *stmt.AltSwitch:
		swStmt := stmt.NewSwitch(n.Cond, n.CaseList)
		swStmt.FreeFloating = n.FreeFloating
		swStmt.Position = n.Position

		*n_ = swStmt

	case *stmt.AltWhile:
		wStmt := stmt.NewWhile(n.Cond, n.Stmt)
		wStmt.FreeFloating = n.FreeFloating
		wStmt.Position = n.Position

		*n_ = wStmt

	default:
		// no change
	}

	return nil // always
}
