<?php

namespace myNamespace;

echo __file__;

echo __Line__;

echo __DIR__;

echo PHP_INT_MAX . PHP_EOL ; 

echo __NAMESPACE__;

function Foo() {
	echo __FUNCTION__;
}

class Bar {
	function Baz() {
		echo __CLASS__ . "::" . __METHOD__	;
		echo Bar::class;
		// echo $this::class;
	}
}

// TODO __TRAIT__