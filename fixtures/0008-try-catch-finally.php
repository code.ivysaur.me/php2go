<?php

function Foo(): int {
	throw new \Exception('error');
	return 0;
}

try {
	Foo();
} catch (\BarException | \BazException $e) {
	echo "bar or baz exception " . $e->getMessage() . "\n";
} catch (\Exception $e) {
	echo "other exception " . $e->getMessage() . "\n";
} finally {
	echo "finally";	
}
