<?php

class Base {
	protected $mX;

	protected function __construct($x) {
		$this->mX = $x;
	}
	
	public function Val(): int {
		return self::TheStatic();
	}
	
	static function TheStatic(): int {
		return 3;
	}
}

class Child extends Base {

	protected $mY;

	public function __construct($x) {
		super($x);
		$this->mY = 0;	
	}
	
	public function Val() {
		return 4;
	}
	
	public function Combine() {
		return $this->Val() + parent::Val();
	}
	
}

echo (new Child(0))->Combine();
