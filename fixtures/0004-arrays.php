<?php

$foo = array(1);

$foo = [2, 3, 4]; // overwrite

$foo[] = 5;

$bar = ["a" => "b", "c" => "d"];
$bar["x"] = 6;

function lookupIndex(): int {
	return 0;
}

echo $bar[lookupIndex() + 1];
