<?php

const GLOBALCONST = 3,
      OTHERCONST = 4;

define('GLOBALDEFINE', 5);

class MyClass {
	const MEMBERCONST = 6;
}

echo GLOBALCONST + OTHERCONST + GLOBALDEFINE + MyClass::MEMBERCONST;
