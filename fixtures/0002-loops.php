<?php

// Different types of loop statement
for($i = 0; $i < 3; ++$i) {
	
	foreach($foo as $k => $v) {
	}
	
	foreach($foo2 as $v2) {
	}
	
	while(true /* infinite. */) {
	}
	
	do {		
	} while (true);
	
}

// Loop with no separate body statement
while (true) echo "hello";

// Loop with multiple initialiser conditions
for ($i = 0, $e = 3; $i < $e; ++$i) {	
}
