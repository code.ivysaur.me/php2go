<?php

// Brace style
if (true) {
	echo "1";
} else if (false) {
	echo "2";
} else if (true && false) {
	echo "3";
} else if (true && false) {
	echo "3";
} else if (true && false) {
	echo "3";
} else if (true && false) {
	echo "3";
} else {
	echo "4";
}

// Colon style
if (1 == 1):
	echo "5";
elseif (2 == 2):
	echo "6";
else:
	echo "7";
endif;

// Unbraced
if (true) echo "100";

// Colon style within deeper scope

{{{{
	if (1 == 1):
		echo "asdf";
	endif;
}}}}