<?php

interface Base {
	function Foo();
}

class Child implements Base {
	function Foo() {}
}
