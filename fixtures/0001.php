<?php

class Bar {
	protected $mX = null;
	protected $mY = 3;
	
	function __construct(string $x) {
		$this->mX = $x;
	}
	
	function hello() {
		echo $this->mX . "\n";
		throw new Exception("asdf");
	}
	
	function scalarThrower() {
		// Comment
		throw "str";
	}
}

function foo($a, int $b): int {
	return 3 + $a + $b;
}

for ($i = 0; $i < 3; ++$i) {
	echo foo($i, 2)."\n";
}

$bb = new Bar("hello");
$bb->hello();
