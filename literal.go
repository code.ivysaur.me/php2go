package main

import (
	"github.com/z7zmey/php-parser/freefloating"
	"github.com/z7zmey/php-parser/node"
	"github.com/z7zmey/php-parser/position"
	"github.com/z7zmey/php-parser/walker"
)

type Literal struct {
	Value string
}

func (l Literal) Walk(v walker.Visitor)                     {}
func (l Literal) Attributes() map[string]interface{}        { return nil }
func (l Literal) SetPosition(p *position.Position)          {}
func (l Literal) GetPosition() *position.Position           { return nil }
func (l Literal) GetFreeFloating() *freefloating.Collection { return nil }

// interface assertion
var _ node.Node = Literal{}
